/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unit.storageelements;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author Tadeáš
 */
public class Warehouse{

    String name;
    ArrayList<Room> rooms;
    Date currentDate;
    
    public Warehouse(String name, ArrayList<Room> rooms, Date startDate){
        this.name = name;
        this.rooms = rooms;
        this.currentDate = startDate;
        
    }
    
    
    public void nextDay(){
        Calendar cal = Calendar.getInstance();
        cal.setTime(currentDate);
        cal.add(Calendar.DATE,1);
        currentDate = cal.getTime();
    }
    
    public String getName() {
        return this.name;
    }

   
    public ArrayList<Room> getRooms() {
        return this.rooms;
    }

    
    public Date getCurrentDate() {
        return this.currentDate;
    }
    
    public ArrayList<Box> getBoxes(String type, boolean coolingType){
       ArrayList<Box> foundBoxes = null; 
        
       for(int i = 0; i < rooms.size() ; i++){
           foundBoxes.addAll(rooms.get(i).getBoxes(type, coolingType));
       }
        return foundBoxes;
    }
    
    public ArrayList<Box> getBoxes(String type, boolean coolingType, int MinimalDaysOfDurability){
        ArrayList<Box> foundBoxes = null; 
        
       for(int i = 0; i < rooms.size() ; i++){
           foundBoxes.addAll(rooms.get(i).getBoxes(type, coolingType,MinimalDaysOfDurability, currentDate));
       }
        return foundBoxes;
    }
    
    public String insertMeat(String type, int count, Date dateOfSlaughter, boolean isFrozen){
        String ret = "{\"item-place\" : [\n\t";

        for (int i = 0; i < rooms.size(); i++) {
            if(rooms.get(i).isFrozen == isFrozen){
                int delta = rooms.get(i).getFreeSpace() - count;
                if(delta >= 0){
                    ret += rooms.get(i).insertMeat(type, count, dateOfSlaughter);
                    count = 0;
                    break;
                }else{
                    ret += rooms.get(i).insertMeat(type, rooms.get(i).getFreeSpace(), dateOfSlaughter);
                    count -= rooms.get(i).getFreeSpace();
                }
            }
        }
        if(count != 0)
            throw new UnsupportedOperationException("warehouse out of capacity!");
            
        ret += "\t]}";
        return ret;
    }
    
    public String reportExpireSoon(){
        
        return null;
    }
    
    public String getReport(){
        
        return null;
    }
    
    public ArrayList<Box> getExpired(){
        
        return null;
    }
    
    public void removeMeat(ArrayList<Box> remove){
        
        
    }
    
    public String removeMeat(String type, int count, boolean isFrozen, int minimalDaysDurability){
        ArrayList<Box> list = getBoxes(type, isFrozen, minimalDaysDurability);
        ArrayList<Box> selected = new ArrayList();
        
        // now we have to sort list, and then put count of boxes into selected and generate string output
        
        
        
        removeMeat(selected);
        return "";
    }
    
    public String getBoxesString(String type, boolean coolingType){
      
       
        
        return null;
    }
}
