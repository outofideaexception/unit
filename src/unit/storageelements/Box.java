/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unit.storageelements;

import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author Tadeáš
 */
public class Box {
    
    private static final int MILLIS_IN_ONE_DAY = 86400000;
    
    private String meatType;
    private Date slaughterDate;
    private boolean isFrozen;
    private Date expirationDate;
    
    public Box(String meatType, Date slaughterDate, boolean isFrozen){
        this.isFrozen = isFrozen;
        this.meatType = meatType;
        this.slaughterDate = slaughterDate;
        Calendar cal = Calendar.getInstance();
        cal.setTime(slaughterDate);
        cal.add(Calendar.DATE,MeatTypes.getExpirateDays(meatType));
        expirationDate = cal.getTime();
    }

    public String getMeatType() {
        return meatType;
    }

    public Date getSlaughterDate() {
        return slaughterDate;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public boolean isFrozen() {
        return isFrozen;
    }
    
    public int getDaysToExpiration(Date currentDate){
        return (int)((expirationDate.getTime() - currentDate.getTime())/MILLIS_IN_ONE_DAY);
    }
    
    public boolean isTheSame(Box b){
        if(b.getExpirationDate().equals(this.expirationDate) && this.meatType == b.getMeatType()){
            return true;
        }
        return false;
    }

}
