package unit.storageelements;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Tadeáš
 */
public class Shelf {

    private String name;
    private int capacity;
    private ArrayList<Box> boxes;
    private boolean isFrozen;

    public Shelf(String name, int capacity, ArrayList<Box> boxes, boolean isFrozen) {
        this.name = name;
        this.capacity = capacity;
        this.boxes = boxes;
        this.isFrozen = isFrozen;
    }

    public String getName() {
        return name;
    }

    public int getCapacity() {
        return capacity;
    }

    public ArrayList<Box> getBoxes() {
        return boxes;
    }

    public int getFreeSpace() {
        return capacity - boxes.size();
    }

    public ArrayList<Box> getBoxes(String type, boolean coolingType) {
        ArrayList<Box> list = new ArrayList();
        for (int i = 0; i < boxes.size(); i++) {
            if (boxes.get(i).getMeatType().equals(type) && boxes.get(i).isFrozen() == coolingType) {
                list.add(boxes.get(i));
            }
        }
        return list;
    }

    public ArrayList<Box> getBoxes(String type, boolean coolingType, int minimalDaysOfDurability, Date currentDate) {
        ArrayList<Box> list = new ArrayList();
        for (int i = 0; i < boxes.size(); i++) {
            if (boxes.get(i).getMeatType().equals(type) && boxes.get(i).isFrozen() == coolingType && boxes.get(i).getDaysToExpiration(currentDate) >= minimalDaysOfDurability) {
                list.add(boxes.get(i));
            }
        }
        return list;
    }

    public void addBox(Box box) {
        boxes.add(box);
        if (boxes.size() > capacity) {
            throw new UnsupportedOperationException("shelf out of capacity!");
        }
    }

    public ArrayList<Box> reportExpireSoon(int maximalDaysToExpiration, Date currentDate) {
        ArrayList<Box> list = new ArrayList();
        for (int i = 0; i < boxes.size(); i++) {
            if (boxes.get(i).getDaysToExpiration(currentDate) <= maximalDaysToExpiration) {
                list.add(boxes.get(i));
            }
        }
        return list;
    }

    public ArrayList<Box> getExpired(Date currentDate) {
        ArrayList<Box> list = new ArrayList();
        for (int i = 0; i < boxes.size(); i++) {
            if (boxes.get(i).getDaysToExpiration(currentDate) < 0) {
                list.add(boxes.get(i));
            }
        }
        return list;
    }

    public void removeMeat(ArrayList<Box> remove) {
        for (int i = 0; i < remove.size(); i++) {
            for (int j = 0; j < boxes.size(); j++) {
                if (boxes.get(j).equals(remove.get(i))) {
                    boxes.remove(j);
                    j--;
                }
            }
        }
    }

    public int getMeatCount(String type, Date date) {
        int ret = 0;
        for (int i = 0; i < boxes.size(); i++) {
            if (boxes.get(i).getMeatType() == type && boxes.get(i).getExpirationDate().equals(date)) {
                ret++;
            }
        }
        return ret;
    }
}
