/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unit.storageelements;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Tadeáš
 */
public class Room {

    String ID;
    boolean isFrozen;
    ArrayList<Shelf> shelves;
    Date currentDate;

    public Room(String ID, boolean isFrozen, ArrayList<Shelf> shelves, Date currentDate) {
        this.ID = ID;
        this.isFrozen = isFrozen;
        this.shelves = shelves;
        this.currentDate = currentDate;
    }

    public boolean isFrozen() {
        return isFrozen;
    }

    public ArrayList getShelves() {
        return shelves;
    }

    public String getRoomId() {
        return ID;
    }

    public ArrayList<Box> getBoxes(String type, boolean coolingType) {
        ArrayList<Box> ret = new ArrayList();
        for (int i = 0; i < shelves.size(); i++) {
            ret.addAll(shelves.get(i).getBoxes(type, coolingType));
        }

        return ret;
    }

    public ArrayList<Box> getBoxes(String type, boolean coolingType, int MinimalDaysOfDurability, Date currentDate) {
        ArrayList<Box> ret = new ArrayList();
        for (int i = 0; i < shelves.size(); i++) {
            ret.addAll(shelves.get(i).getBoxes(type, coolingType, MinimalDaysOfDurability, currentDate));
        }
        return ret;
    }

    public String insertMeat(String type, int count, Date dateOfSlaughter) {
        String ret = "";

        for (int i = 0; i < shelves.size(); i++) {
            int spaceLeft = shelves.get(i).getCapacity() - shelves.get(i).getBoxes().size();
            int inserted = 0;
            if (spaceLeft > 0) {

                for (int j = spaceLeft; j > 0; j--) {
                    shelves.get(i).addBox(new Box(type, dateOfSlaughter, this.isFrozen));
                    count--;
                    inserted++;
                    if (count == 0) {
                        break;
                    }

                }
                String help = "{\"Box-number\" : \"" + ID + "\"shelf-number\" : \"" + shelves.get(i).getName() + "\",\"count\" : " + inserted + "},\n";
                ret = ret + help;
            }
            if (count == 0) {
                break;
            }
        }

        return ret;
    }

    public int getFreeSpace() {
        int ret = 0;
        for (int i = shelves.size(); i > 0; i--) {
            ret = ret + shelves.get(i).getFreeSpace();
        }
        return ret;
    }

    public ArrayList<Box> reportExpireSoon(Date currentDate) {
        ArrayList<Box> ret = new ArrayList();
        for (int i = 0; i < shelves.size(); i++) {
            ret.addAll(shelves.get(i).reportExpireSoon(10, currentDate));
        }
        return ret;
    }

    public ArrayList<Box> getReport() {
        ArrayList<Box> ret = new ArrayList();
        for (int i = 0; i < shelves.size(); i++) {
            ret.addAll(shelves.get(i).getBoxes());
        }
        return ret;
    }

    public ArrayList<Box> getExpired(Date currentDate) {
        ArrayList<Box> ret = new ArrayList();
        for (int i = 0; i < shelves.size(); i++) {
            ret.addAll(shelves.get(i).getExpired(currentDate));
        }
        return ret;
    }

    public void removeMeat(ArrayList<Box> remove) {
        for (int i = 0; i < shelves.size(); i++) {
            shelves.get(i).removeMeat(remove);
        }
    }

    public String getBoxesString(String type) {
        System.out.println("qerwef");
        String ret = "";
        
        for (int i = 0; i < shelves.size(); i++) {
            System.out.println("sefewf");
            ArrayList<Box> correct = shelves.get(i).getBoxes(type, isFrozen);
            if (correct.size() < 0) {
                for (int j = 0; j < correct.size(); j++) {
                    for (int h = 1; h < correct.size(); j++) {
                        if (correct.get(j).isTheSame(correct.get(h))) {
                            correct.remove(h);
                        }
                    }
                }
            }
            
            for(int j = 0; j< correct.size();j++){
            String help = "{\"Box-number\" : \"" + ID + "\"shelf-number\" : \"" + shelves.get(i).getName() + "\",\"count\" : " + shelves.get(i).getMeatCount(type, correct.get(j).getExpirationDate()) + "},\n";
            ret = ret+help;
            }
        }

        return ret;
    }

}
